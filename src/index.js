import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import App from './App.vue';
import Tiles from './Tiles.vue';
import Article from './Article.vue';
import About from './About.vue';
import Whatever from './Whatever.vue';
import NotFound from './NotFound.vue';

const routes = [
  {
    path: '/',
    name: 'tiles',
    component: Tiles
  }, {
    path: '/article/:id',
    name: 'article',
    component: Article
  }, {
    path: '/about',
    name: 'about',
    component: About
  }, {
    path: '/whatever',
    name: 'whatever',
    component: Whatever,
  }, {
    path: '*',
    name: 'NotFound',
    component: NotFound,
  }
];

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueResource);

const store = new Vuex.Store({
  state: {
    tiles: [],
    errorLoadingTiles: false,
  }
});

const router = new VueRouter({ routes });

const app = new Vue({
  router,
  store,
  render(h) {
    return h(App);
  },
});

app.$mount(document.querySelector('#app'));
