const {join, resolve} = require('path');
const {readFile, createReadStream} = require('fs');

const webpack = require('webpack');

const HtmlPlugin = require('html-webpack-plugin');
const HtmlTemplatePlugin = require('html-webpack-template');

module.exports = {
  context: __dirname,

  entry: join(__dirname, 'src/index.js'),

  output: join(__dirname, 'public/bundle.js'),

  resolve: {
    modules: [
      resolve('./src/'),
      resolve('./node_modules'),
    ],
  },

  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader',
      },

      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      }
    ],
  },

  plugins: [
    new HtmlPlugin({
      filename: 'index.html',
      template: HtmlTemplatePlugin,
      inject: false,
      mobile: true,
      appMountId: 'app',
    }),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: `"${process.env.NODE_ENV}"`,
      },
    }),
  ],

  devServer: {
    contentBase: './public/',
    hot: true,
    port: 9000,
    setup(app) {
      app.get('/api/tiles', function (req, res) {
        res.writeHead(200, {'Content-Type': 'application/json'});
        createReadStream(join(process.cwd(), 'api/tiles.json'), {encoding: 'utf-8'})
          .pipe(res);
      });

      app.get('/api/tiles/:id', function (req, res) {

        if (!req.params.id) {
          res.send({
            error: "invalid parameter id"
          });
        }

        readFile(join(process.cwd(), 'api/tiles.json'), 'utf8', function (err, data) {

          if (err || !data) {
            res.send({
              error: "error read file"
            });
          }

          try {
            let tilesData = JSON.parse(data);
            if (tilesData
              && tilesData.tiles
              && tilesData.tiles.length) {

              item = tilesData.tiles.filter(a => a.id == req.params.id);

              if (item
                  && item.length == 1) {
                res.send( {
                  data: item[0]
                });
              } else {
                res.send({
                  error: "record not found"
                });
              }
            }
          } catch {
            res.send({
              error: "can't parse json file"
            });
          };
        });
      });
    },
  },
};
